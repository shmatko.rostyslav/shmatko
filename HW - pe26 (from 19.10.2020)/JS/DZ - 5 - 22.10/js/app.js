"use strict";

// Экранирование нужно, чтобы использовать некоторые символы просто, как символы, без значения для кода в программировании

// function fio(Firstname, Secondname, Birthdate) {
//     prompt("Enter first nmae");
//     prompt("Enter second");
//     prompt("Enter you birth date - format dd.mm.yyyy")
//     return {
//         first: Firstname,
//         second: Secondname,
//         birth: Birthdate
//         write: function (writeOut) {
//             this.first.charAt(0).toLowerCase() + this.second.toLowerCase();
//         }
//
//     };
// }
//
// console.log(fio());

function createNewUser() {
    this.userFirstName = prompt("Tell me your First name");
    this.userSecondName = prompt("Tell me your Second name");
    this.userDate = prompt("Put your birthdate in format dd.mm.yyyy");
    this.getLogin = function () {
        let enteredName = this.userFirstName.charAt(0).toLowerCase() + this.userSecondName.toLowerCase();
        return enteredName;
    }
    this.getAge = function () {
        let dateNow = new Date();
        let birthDate = Date.parse(this.userDate.split(".").reverse().join("."));
        this.age = Math.floor((dateNow - birthDate) / (1000 * 60 * 60 * 24 * 365))
        return this.age;
    }
    this.getPassword = function () {
        this.password = this.userFirstName.charAt(0).toUpperCase() + this.userSecondName.toLowerCase() + this.userDate.split(".")[2];
        return this.password
    }
};

let newUser = new createNewUser();

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
