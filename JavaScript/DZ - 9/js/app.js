const incrBtn = document.querySelector('.item-btn-incr');
const decrBtn = document.querySelector('.item-btn-decr');
const counter = document.querySelector('.item-counter');
const changeCounter = (e) => {
    // const counterValue = +counter.textContent; - строка на элемент след.строки counterValue + 1;
    counter.textContent = counterValue + 1;
};

incrBtn.addEventListener('click', changeCounter);

const container = document.createElement('div');
const text = document.createElement('p');

let htmlCode = '<button>1</button>\n<button>2</button>\n<button>3</button>\n <button>4</button>\n<button>5</button>';

container.insertAdjacentHTML("afterbegin", htmlCode);
container.prepend(text);

container.addEventListener('click', (ev) => {
    debugger
    if (ev.target.tagName.toLowerCase() === "button") {
        let content = ev.target.textContent;
        text.textContent = content;
    }
});

document.body.prepend(container);


const container = document.createElement('div');
const text = document.createElement('p');

container.addEventListener('click', ev => {
    let id = ev.target.dataset.toggleId;
    if (!id) return;

    let listItem = document.getElementById(id);

    listItem.hidden = !listItem.hidden;

});

