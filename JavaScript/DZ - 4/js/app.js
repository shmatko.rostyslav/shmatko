"use strict";

// Методы - это функции, встроенные в объект. С их помощью мы можем передавать целые функции и значения в них в нужный объект.

function createNewUser() {
    this.userFirstName = prompt("Enter first name");
    this.userSecondName = prompt("Enter Second Name")
    this.getLogin = function () {
        let enteredName = this.userFirstName.charAt(0).toLowerCase() + this.userSecondName.toLowerCase();
        return enteredName;
    }
}
let newUser = new createNewUser();
console.log(newUser.getLogin());