"use strict";

//Экранирование в программировании нужно для того, чтобы использовать особые символы просто, как символы.
//Если символ имеет специальное назначение в программировании, то с помощью экранирования можно использовать его просто как обычный символ.


function createNewUser() {
    this.userFirstName = prompt("Enter first name");
    this.userSecondName = prompt("Enter Second Name");
    this.userDate = prompt("Введите дату рождения в формате: dd.mm.yyyy")
    this.getLogin = function () {
        let enteredName = this.userFirstName.charAt(0).toLowerCase() + this.userSecondName.toLowerCase();
        return enteredName;
    }
    this.getAge = function () {
        let dateNow = new Date();
        let birthDate = Date.parse(this.userDate.split(".").reverse().join("."));
        this.age = Math.floor((dateNow - birthDate) / (1000 * 60 * 60 * 24 * 365))
        return this.age;
    }
    this.getPassword = function () {
        this.password = this.userFirstName.charAt(0).toUpperCase() + this.userSecondName.toLowerCase() + this.userDate.split(".")[2];
        return this.password
    }
}
let newUser = new createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword())