"use strict";

// Функции в программировании нужны для более чистого кода. Можно записать определенные действия в функцию один раз, чтобы после просто вызывать ее. Это сокращает код.
// Передача аргументов в функцию нужна, чтобы задавать необходимые значения и передавать их в функцию, а не писать под каждые значения функцию заново.

let userNumber1 = +prompt("Введите первое число");
let userNumber2 = +prompt("Введите второе число");
let userOperation = prompt("Введите необходимую операцию: +, -, * или /");

function finaloperation(number1, operation, number2) {
    switch (operation) {
        case "+":
            return number1 + number2;
        case "-":
            return number1 - number2;
        case "*":
            return number1 * number2;
        case "/":
            return number1 / number2;
    }
}

console.log(finaloperation(userNumber1, userOperation, userNumber2));
