"use strict";

//https://dan-it.gitlab.io/fe-book/programming_essentials/javascript/ext_modifying-document/ext_modifying-document.html

function listCreator (list) {
    let newList = list.map(item => `<li>${item}</li>`);
    let newUl = document.createElement("ul");
    newList.forEach(item => {
        newUl.insertAdjacentHTML("beforeend", item);
    })
    document.body.append(newUl);
}

listCreator(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
