// цикл forEach перебирает каждый элемент внутри массива

function filterBy(arr, type) {
    let arrayList = [];
        for (let i = 0; i < arr.length; i++) {
            if (typeof arr[i] !== type) {
                arrayList.push(arr[i]);
            }
    }
    console.log(typeof(arrayList));
    return arrayList;
};

console.log(filterBy(['hello', 'world', 23, '23', null], "string"));