// Document Object Model (DOM) - это объектная модель документа, рассматривающая все HTML элементы, как объекты и хранящая в себе информацию обо всем документе.

function listCreator(list) {
    let newList = list.map(item => `<li>${item}</li>`);
    let parent = document.createElement('ul');
    newList.forEach(item => {
        parent.insertAdjacentHTML("beforeend", item);
    });
    document.body.prepend(parent);
};

listCreator(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);